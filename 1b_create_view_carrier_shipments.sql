-- 1b_create_view_carrier_shipments

CREATE VIEW carrier_shipments AS
SELECT s.tracking_number, s.weight, s.status, c.name AS carrier_name
FROM shipments s
INNER JOIN carriers c ON s.carrier_id = c.carrier_id;

SELECT * FROM carrier_shipments
