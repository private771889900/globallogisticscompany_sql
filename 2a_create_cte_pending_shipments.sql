-- 2a_create_cte_pending_shipments

WITH PendingShipmentsCTE AS (
    SELECT
        s.tracking_number, s.weight,
        w.location AS warehouse_location
    FROM
        shipments s
    INNER JOIN
        warehouses w ON s.warehouse_id = w.warehouse_id
    WHERE
        s.status = 'Pending'
)
SELECT * FROM PendingShipmentsCTE;