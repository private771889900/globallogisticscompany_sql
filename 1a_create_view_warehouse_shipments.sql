-- 1a_create_view_warehouse_shipments  

CREATE VIEW warehouse_shipments AS
SELECT s.tracking_number, s.weight, s.status, w.name AS warehouse_name
FROM shipments s
INNER JOIN  warehouses w on s.warehouse_id = w.warehouse_id;

SELECT * FROM warehouse_shipments