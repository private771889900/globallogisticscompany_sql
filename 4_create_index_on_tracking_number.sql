-- create_index_on_tracking_number

CREATE INDEX idx_shipments_tracking_number ON shipments(tracking_number);
