-- 2b_create_CTE_Heavy_Shipments

WITH HeavyShipmentsCTE AS (
    SELECT
        s.tracking_number, s.weight,
        c.name AS carrier_name
    FROM
        shipments s
    INNER JOIN
        carriers c ON s.carrier_id = c.carrier_id
    WHERE
        s.weight > 200
)
SELECT * FROM HeavyShipmentsCTE;