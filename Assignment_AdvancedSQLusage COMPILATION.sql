-- 0a_create_tables

CREATE TABLE warehouses (
    warehouse_id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    location VARCHAR(100) NOT NULL
);

CREATE TABLE carriers (
    carrier_id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    contact_person VARCHAR(50),
    contact_number VARCHAR(15)
);

CREATE TABLE shipments (
    shipment_id SERIAL PRIMARY KEY,
    tracking_number VARCHAR(20) UNIQUE NOT NULL,
    weight DECIMAL(10, 2) NOT NULL,
    status VARCHAR(20) DEFAULT 'Pending',
    warehouse_id INT REFERENCES warehouses(warehouse_id),
    carrier_id INT REFERENCES carriers(carrier_id)
);

-- 0b_insert_seed_data

INSERT INTO warehouses (name, location) VALUES
    ('Warehouse A', 'Location A'),
    ('Warehouse B', 'Location B');

INSERT INTO carriers (name, contact_person, contact_number) VALUES
    ('Carrier X', 'John Doe', '123-456-7890'),
    ('Carrier Y', 'Jane Smith', '987-654-3210');

INSERT INTO shipments (tracking_number, weight, warehouse_id, carrier_id) VALUES
    ('ABC123', 150.5, 1, 1),
    ('XYZ789', 200.0, 2, 2);
	
-- 1a_create_view_warehouse_shipments  

CREATE VIEW warehouse_shipments AS
SELECT s.tracking_number, s.weight, s.status, w.name AS warehouse_name
FROM shipments s
INNER JOIN  warehouses w on s.warehouse_id = w.warehouse_id;

SELECT * FROM warehouse_shipments

-- 1b_create_view_carrier_shipments

CREATE VIEW carrier_shipments AS
SELECT s.tracking_number, s.weight, s.status, c.name AS carrier_name
FROM shipments s
INNER JOIN carriers c ON s.carrier_id = c.carrier_id;

SELECT * FROM carrier_shipments

-- 2a_create_cte_pending_shipments

WITH PendingShipmentsCTE AS (
    SELECT
        s.tracking_number, s.weight,
        w.location AS warehouse_location
    FROM
        shipments s
    INNER JOIN
        warehouses w ON s.warehouse_id = w.warehouse_id
    WHERE
        s.status = 'Pending'
)
SELECT * FROM PendingShipmentsCTE;

-- 2b_create_cte_heavy_shipments

WITH HeavyShipmentsCTE AS (
    SELECT
        s.tracking_number, s.weight,
        c.name AS carrier_name
    FROM
        shipments s
    INNER JOIN
        carriers c ON s.carrier_id = c.carrier_id
    WHERE
        s.weight > 200
)
SELECT * FROM HeavyShipmentsCTE;

-- 3a_update_shipment_status_transaction

BEGIN;

UPDATE shipments
SET status = 'Shipped', weight = weight + 10
WHERE tracking_number = 'ABC123';

-- 3b_insert_new_shipment_transaction

INSERT INTO shipments (tracking_number, weight, status, warehouse_id, carrier_id)
VALUES ('LMN456', 180.75, 'Pending', 2, 2);

COMMIT;

-- create_index_on_tracking_number

CREATE INDEX idx_shipments_tracking_number ON shipments(tracking_number);