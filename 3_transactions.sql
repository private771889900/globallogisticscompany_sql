-- 3a_update_shipment_status_transaction

BEGIN;

UPDATE shipments
SET status = 'Shipped', weight = weight + 10
WHERE tracking_number = 'ABC123';

-- 3b_insert_new_shipment_transaction

INSERT INTO shipments (tracking_number, weight, status, warehouse_id, carrier_id)
VALUES ('LMN456', 180.75, 'Pending', 2, 2);

COMMIT;